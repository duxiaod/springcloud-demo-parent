package cn.duxiaod.springcloudeurekaclient.controller;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EurekaClientController {
    @Value("${server.port}")
    private int port;

    @GetMapping(path = "api/userInfo/{uid}")
    public String test1(@PathVariable("uid")String uid){
        return "uid:"+uid+" from eureka client port:"+port;
    }
}
