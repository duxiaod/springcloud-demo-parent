package cn.demo.entity;

import lombok.Data;

@Data
public class Account {
    private int id;
    private int balance;
    private String name;

    public Account() {

    }
    public Account(int balance, String name) {
        this.balance = balance;
        this.name = name;
    }
    public Account(int id, int balance, String name) {
        this.id = id;
        this.balance = balance;
        this.name = name;
    }

}
