package cn.demo.mapper;

import cn.demo.entity.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AccountMapper extends BaseMapper<Account> {

}
