## MySql-索引
```sql
## 新建索引
create index idx_user_name on tb_user(name);
## 删除索引
drop index idx_user_name on tb_user;
```
1. 表的主外键必须要有索引；
2. 数据量超过300的表应该建立索引；
3. 经常与其他表进行关联的表，在连接字段上应该建立索引；
4. 经常出现在where子句中的字段，特别是大表字段，应建立索引；
5. 索引应该建立在选择性高的字段上；
6. 索引应该建立在小字段上，对于大文本字段甚至超长字段，不应建立索引；
7. 复合索引的建立需要进行仔细分析；尽量考虑用单字段索引代替；
- 正确选择符合索引中的主列字段，一般是选择性较好的字段；
- 复合索引的多个字段是否经常以AND的形式出现在where子句中？单字段查询是否  极少甚至没有？如果是，则可以建立复合索引；否则考虑单字段索引；
- 如果复合索引中包含字段经常单独出现在where子句中，则分解为多个单字段索引；
- 如果复合索引包含字段超过3各，那么需要考虑其必要性，减少复合索引字段；
- 如果既有单字段的索引，又有这几个字段上的复合索引，一般可以删除复合索引；
8. 频繁进行数据操作的表，不要建立太多的索引；
9. 应删除无用的索引，避免对执行计划造成负面影响；
## 索引失效场景
1. 负向查找不能使用索引
```sql
select * from order where status!=0 and stauts!=1
```
还有 not in/not exists都不是好习惯
```sql
select name from order where status not in (0,1);
```
可以优化为in查询：
```sql
select * from order where status in(2,3)
```
2. 前导模糊查询不能使用索引
```sql
select name from user where name like '%xxx'
```
非前导则可以</br>
3. 数据区分不明显不建议建立索引</br>
   如 tb_user 表中的性别字段，可以明显区分的才建议创建索引，如身份证等字段。
   经验上，能过滤80%数据时就可以使用索引。对于订单状态，如果状态值很少，不宜使用索引，如果状态值很多，能够过滤大量数据，则应该建立索引。</br>
4. 字段的默认值不要为null
5. 在属性上进行计算不能命中索引
```sql
select * from order where YEAR(date) < = '2017'
```
即使date上建立了索引，也会全表扫描，可优化为值计算：
```sql
select * from order where date < = CURDATE()
```
6. 复合索引最左前缀</br>
   用户中心建立了(login_name, passwd)的复合索引</br>
```sql
select * from user where login_name=? and passwd=?
select * from user where passwd=? and login_name=?
```
但是使用</br>
```sql
select * from user where passwd=?
```
不能命中索引，不满足复合索引最左前缀</br>
7. 如果明确知道只有一条记录返回
```sql
select name from user where username='xxxx' limit 1
```
提高效率，可以让数据库停止游标移动，停止全表扫描。</br>
8. 强制类型转换会全表扫描
```sql
select * from user where phone=13800000000
```
这样虽然可以查出数据，但会导致索引失效。</br>
需要修改为</br>
```sql
select * from user where phone='13800000000'
```
9. 把计算放到业务层而不是数据层，除了节省数据的CPU，还有意想不到的查询缓存优化效果
10. 排序的索引问题</br>
mysql查询只使用一个索引，因此在where子句中已经使用了索引了，那么order by中的列是不会使用索引的，因此数据库的默认排序符合要求的情况下不要使用排序操作；尽量不要包含多个列的排序，如果需要最好给这些列创建复合索引
