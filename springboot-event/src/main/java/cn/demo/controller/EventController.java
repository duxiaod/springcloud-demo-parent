package cn.demo.controller;

import cn.demo.event.TestEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("event")
public class EventController {
    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @GetMapping("/test")
    public String list(){
        eventPublisher.publishEvent(new TestEvent(this, "{name:'test',code:1}"));
        return "test";
    }

}
