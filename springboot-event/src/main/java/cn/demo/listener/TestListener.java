package cn.demo.listener;

import cn.demo.event.TestEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
@Component
public class TestListener {

    @EventListener
    public void testListener(TestEvent testEvent){
        System.out.println(testEvent.getMessage());
        System.out.println("testListener   1");
    }


    @EventListener
    public void testListener2(TestEvent testEvent){
        System.out.println(testEvent.getMessage());
        System.out.println("testListener   2");
    }
}
