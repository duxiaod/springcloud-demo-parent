package cn.duxiaod.springbootdubboconsumer.service.impl;

import cn.duxiaod.dubbo.api.service.CostService;
import cn.duxiaod.springbootdubboconsumer.service.ProductService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

/**
 * 产品Service
 */
@Service(value = "productServiceImpl2")
public class ProductServiceImpl2 implements ProductService {

    /**
     * 使用dubbo的注解 org.apache.dubbo.config.annotation.DubboReference。进行远程调用service
     */
    @Reference(version = "${dubbo.service.version2}")
    private CostService costService;

    @Override
    public Integer getCost(int a) {
        return costService.add(a);
    }
}
