package cn.duxiaod.springbootdubboconsumer.controller;

import cn.duxiaod.dubbo.api.service.CostService;
import cn.duxiaod.springbootdubboconsumer.service.ProductService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class ProductController {
    @Resource(name = "productServiceImpl")
    private ProductService productServiceImpl;

    @Resource(name = "productServiceImpl2")
    private ProductService productServiceImpl2;

    @Reference(version = "${dubbo.service.version2}")
    private CostService costService;
    /**
     * 添加完 返回总共消费
     * @param a
     * @return
     */
    @HystrixCommand(fallbackMethod = "fallbackMethod")
    @RequestMapping("/add")
    public String getCost(int a){
        return "该产品总共消费 ："+productServiceImpl.getCost(a);
    }

    @RequestMapping("/add2")
    public String getCost2(int a){
        return "该产品总共消费 ："+productServiceImpl2.getCost(a);
    }

    @RequestMapping("/add3")
    public String getCost3(int a){
        return "该产品总共消费 ："+costService.add(a);
    }

    public String fallbackMethod(int a){
        return "Hystrix fallback";
    }
}
