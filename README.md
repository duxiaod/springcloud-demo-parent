# springcloud demo
***
## 项目说明
1. springcloud-eureka-server注册中心eureka
2. springcloud-eureka-client客户端
3. springcloud-feign应用服务快速失败熔断降级保护 Hystrix
4. springcloud-ribbon应用服务快速失败熔断降级保护 Hystrix
5. springcloud-dashboard服务响应性能成功率监控 Hystrix
6. springcloud-turbine监控信息聚合展示 Hystrix
7. springcloud-zuul 服务网关Zuul
8. springcloud-zuul1 服务网关Zuul 动态路由与权限过滤器
9. springcloud-gateway gateway网关
10. springcloud-common 公共依赖
11. springboot-dubbo-api api项目
12. springboot-dubbo-consumer dubbo消费者集成Hystrix
13. springboot-dubbo-provider dubbo提供者Hystrix
14. [springcloud-alibaba-sentinel-demo 集成sentinel](#集成sentinel)
15. [springcloud-alibaba-nacos-demo 集成nacos作为注册中心和配置中心](#nacos作为注册中心和配置中心)
16. [springboot-mybatisplus-demo 集成mybatisplus](#springboot集成mybatisplus)
17. [springboot-mybatis-demo 集成mybatis](#springboot集成mybatis)
18. [springboot-jpa-demo 使用jpa](#springboot使用jpa)
19. [springboot-rabbitmq-demo 使用rabbitmq](#springboot使用rabbitmq)
20. [spring-security-demo 使用SpringSecurity](#SpringSecurity)
21. [springcloud-sso-server 使用Spring Security OAuth2 SSO](#SpringSecurityOAuth2SSO)
22. [springboot-retry 使用springboot-retry重试机制](#springboot-retry重试机制)
23. [springboot-tx-demo springboot Transactional](#SpringTransactional)
24. [springboot-aop-log](#springboot-aop-log)
25. [springboot-redis-demo使用redis缓存](#springboot-redis-demo使用redis缓存)
26. [java-base](#java-base)

## 集成sentinel
1. [https://github.com/alibaba/Sentinel/releases](https://github.com/alibaba/Sentinel/releases) 在这个地址，下载release的jar，然后启动即可。
2. 使用以下命令启动sentinel服务(我使用了1.8.0)
```java
java -Dserver.port=8718 -Dcsp.sentinel.dashboard.server=localhost:8718 -Dproject.name=sentinel-dashboard -Dcsp.sentinel.api.port=8719 -jar sentinel-dashboard-1.8.0.jar
```
3. 访问[http://localhost:8718/](http://localhost:8718/)   使用：sentinel/sentinel登录控制台
4. 新建maven项目，引入sentinel依赖
```
<dependency>
            <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>

<!--sentinel-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
</dependency>
<!--sentinel end-->
```
5. 在项目resources目录下新建application.yml文件
```
server:
  port: 8063

spring:
  application:
    name: springcloud-alibaba-sentinel-demo
  cloud:
    sentinel:
      transport:
        dashboard: localhost:8718
```
6. 新建启动类
```java
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcloudAlibabaSentinelApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringcloudAlibabaSentinelApplication.class,args);
    }
}
```
6. 新建测试方法
```java
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringcloudAlibabaSentinelController {
    @GetMapping(value = "/hello")
    public String hello() {
        return "Hello Sentinel";
    }
}
```
7. 访问测试方法
![输入图片说明](https://images.gitee.com/uploads/images/2021/0414/104909_a14a959c_1045447.png "1.png")
8. 查看sentinel控制台，可以看到服务
![输入图片说明](https://images.gitee.com/uploads/images/2021/0414/105119_687b3dc8_1045447.png "2.png")
9. 设置流控规则，并验证
![输入图片说明](https://images.gitee.com/uploads/images/2021/0414/105407_8945c525_1045447.png "3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0414/105424_19c8a59e_1045447.png "4.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0414/105438_dd42a313_1045447.png "5.png")

## nacos作为注册中心和配置中心
1. 下载安装nacos
可以从[https://github.com/alibaba/nacos/releases](https://github.com/alibaba/nacos/releases)下载nacos-server-$version.zip包。
Windows下载解压后（.zip），直接点击bin/startup.cmd -m standalone就可以了。
Nacos默认是集群模式cluster，可以startup.cmd属性MODE为单机模式standalone
```
set MODE="standalone"
```
2. 使用用户nacos/nacos登录nacos管理台，新增配置
![输入图片说明](https://images.gitee.com/uploads/images/2021/0414/170646_02e238a0_1045447.png "8.png")
3. 新建maven项目，引入相关依赖
```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<!-- SpringCloud Ailibaba Nacos -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
</dependency>
<!-- SpringCloud Ailibaba Nacos Config -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
</dependency>
<!-- SpringCloud Ailibaba Sentinel -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
</dependency>
<!-- SpringBoot Actuator -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```
4. 在项目resources目录下新建bootstrap.yml(注意区别)文件
```
server:
  port: 8064

# Spring
spring:
  application:
    # 应用名称
    name: springcloud-alibaba-nacos-demo
  profiles:
    # 环境配置
    active: dev
  main:
    allow-bean-definition-overriding: true
  cloud:
    nacos:
      discovery:
        # 服务注册地址
        server-addr: 127.0.0.1:8848
      config:
        # 配置中心地址
        server-addr: 127.0.0.1:8848
        # 配置文件格式
        file-extension: yml
        # 共享配置
        shared-configs:
          - application-${spring.profiles.active}.${spring.cloud.nacos.config.file-extension}
    sentinel:
      # 取消控制台懒加载
      eager: true
      transport:
        # 控制台地址
        dashboard: 127.0.0.1:8718
```
5. 新建启动类

```java
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudAlibabaNacosApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringCloudAlibabaNacosApplication.class,args);
    }
}
```
6. 新建测试方法

```java
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class TestController {
    @Value("${test.enabled}")
    private String testEnabled;

    @GetMapping("test")
    public String test(){
        return "test.enabled="+testEnabled;
    }
}
```
7. 测试
![输入图片说明](https://images.gitee.com/uploads/images/2021/0414/171146_8e227e3b_1045447.png "1.png")
8. nacos中刷新配置
![输入图片说明](https://images.gitee.com/uploads/images/2021/0414/171243_4f20feac_1045447.png "2.png")
9. 再次测试
![输入图片说明](https://images.gitee.com/uploads/images/2021/0414/171320_26476ae2_1045447.png "3.png")

## springboot集成mybatisplus
1. 参考官方网站[https://mp.baomidou.com/guide/quick-start.html#%E5%88%9D%E5%A7%8B%E5%8C%96%E5%B7%A5%E7%A8%8B](https://mp.baomidou.com/guide/quick-start.html#%E5%88%9D%E5%A7%8B%E5%8C%96%E5%B7%A5%E7%A8%8B)

## springboot集成mybatis

## springboot使用jpa

## springboot使用rabbitmq

## SpringSecurity

## SpringSecurityOAuth2SSO

## springboot-retry重试机制
1. @Retryable：标记当前方法会使用重试机制
2. value：重试的触发机制，当遇到Exception异常的时候，会触发重试
3. maxAttempts：重试次数（包括第一次调用）
4. delay：重试的间隔时间
5. multiplier：delay时间的间隔倍数
6. maxDelay：重试次数之间的最大时间间隔，默认为0，如果小于delay的设置，则默认为30000L
7. @Recover：标记方法为回调方法，传参与@Retryable的value值需一致

## SpringTransactional
### 1、编程式事务；声明式事务
### 2、propagation，表示事务传播机制，默认为Propagation.REQUIRED，其他详细属性如下：
1. Propagation.REQUIRED：如果当前存在事务，则加入该事务；如果当前事务不存在则新建事务
2. Propagation.SUPPORTS：如果当前存在事务，则加入该事务；如果当前不存在事务，则以非事务方式继续运行
3. Propagation.MANDATORY：如果当前存在事务，则加入该事务；如果当前不存在事务，则抛出异常
4. Propagation.REQUIRES_NEW：重新创建一个新事务，如果当前存在事务，则暂停当前事务。（当类A的方法a用默认Propagation.REQUIRED模式，类B中的方法b用Propagation.REQUIRES_NEW模式，然后在a方法中调用b方法操作数据库，然后a方法抛出异常后，b方法并没有回滚，因为Propagation.REQUIRES_NEW会暂停a方法的事务）
5. Propagation.NOT_SUPPORTED：以非事务的方式运行，如果当前存在事务则暂停当前事务。
6. Propagation.NEVER：以非事务的方式运行，如果当前存在事务则抛出异常
7. Propagation.NESTED：效果同Propagation.REQUIRED
### 3、事务失效场景
1. @Transactional 应用在非public修饰的方法上
2. 数据库引擎不支持事务
3. propagation事务传播属性设置错误
4. rollbackFor设置错误
5. 方法之间互相调用
6. 异常被catch”吃了“
7. spring事务和业务逻辑代码必须在一个线程中

## springboot-aop-log
aop实现记录日志

## springboot-redis-demo使用redis缓存
1. @EnableCaching：注解开启事务
2. @Cacheable(cacheNames="user_key", key="#id")
3. @Caching(evict = {@CacheEvict(value = "user_key",allEntries = true),
   @CacheEvict(value = "user_key_value",allEntries = true)})

## java-base
