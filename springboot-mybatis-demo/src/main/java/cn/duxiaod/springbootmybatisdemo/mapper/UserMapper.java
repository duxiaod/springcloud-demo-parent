package cn.duxiaod.springbootmybatisdemo.mapper;

import cn.duxiaod.springbootmybatisdemo.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    void insertUser(User user);

    List<User> findList(User user);
}
