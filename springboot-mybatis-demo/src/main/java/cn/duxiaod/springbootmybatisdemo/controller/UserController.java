package cn.duxiaod.springbootmybatisdemo.controller;

import cn.duxiaod.springbootmybatisdemo.entity.User;
import cn.duxiaod.springbootmybatisdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "findList")
    public List<User> index(User user){
        return userService.findList(user);
    }

}
