package cn.duxiaod.springbootmybatisdemo.service;

import cn.duxiaod.springbootmybatisdemo.entity.User;
import cn.duxiaod.springbootmybatisdemo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public void insertUser(User user) {
        userMapper.insertUser(user);
    }

    public List<User> findList(User user) {
        return userMapper.findList(user);
    }
}
