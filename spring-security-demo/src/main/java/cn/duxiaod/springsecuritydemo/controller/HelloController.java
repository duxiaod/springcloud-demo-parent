package cn.duxiaod.springsecuritydemo.controller;

import cn.duxiaod.common.domian.R;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @GetMapping("/hello")
    public R hello(){
        return R.ok("hello spring security");
    }

    @GetMapping("/index")
    public R index(Authentication authentication){
        return R.ok(authentication);
    }

    @GetMapping("/auth/admin")
    @PreAuthorize("hasAuthority('admin')")
    public String authenticationTest() {
        return "您拥有admin权限，可以查看";
    }
}
