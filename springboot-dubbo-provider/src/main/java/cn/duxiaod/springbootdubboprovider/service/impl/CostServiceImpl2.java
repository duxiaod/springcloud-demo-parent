package cn.duxiaod.springbootdubboprovider.service.impl;

import cn.duxiaod.dubbo.api.service.CostService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.apache.dubbo.config.annotation.Service;

import java.util.Random;

/**
 * 注意使用alibaba注解：org.apache.dubbo.config.annotation.DubboService
 */
@Service(version = "${dubbo.service.version2}")
public class CostServiceImpl2 implements CostService {
    /**
     * 假设之前总花费了100
     */
    private final Integer totalCost = 1000;

    /**
     * 之前总和 加上 最近一笔
     * @param cost
     * @return
     */
    @HystrixCommand(commandProperties = {
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "2000")
    })
    @Override
    public Integer add(int cost) {
        System.out.println("版本2");
        return totalCost + cost;
    }
}
