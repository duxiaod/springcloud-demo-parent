package cn.duxiaod.springbootdubboprovider;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@SpringBootApplication
@EnableDubbo
@EnableHystrix
public class SpringbootDubboProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootDubboProviderApplication.class,args);
    }
}
