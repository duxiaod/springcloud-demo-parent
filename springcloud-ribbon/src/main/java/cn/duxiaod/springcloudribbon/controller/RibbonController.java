package cn.duxiaod.springcloudribbon.controller;

import cn.duxiaod.springcloudribbon.service.RibbonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RibbonController {
    @Autowired
    private RibbonService ribbonService;

    @GetMapping(value = "api/userInfo/{uid}")
    public String userInfo(@PathVariable("uid") String uid){
        return ribbonService.userInfo(uid)+" from ribbon";
    }
    @GetMapping(value = "api2/userInfo/{uid}")
    public String userInfo2(@PathVariable("uid") String uid){
        return ribbonService.userInfo(uid)+" from ribbon api2";
    }
}
