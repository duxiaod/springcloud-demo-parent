package cn.duxiaod.springcloudribbon.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RibbonService {
    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "userInfoFallback")
    public String userInfo(String uid){
        return restTemplate.getForObject("http://SPRINGCLOUD-EUREKA-CLIENT/api/userInfo/"+uid,String.class);
    }

    public String userInfoFallback(String uid) {
        return "queryUserInfo by userId：" + uid + " err！from ribbon hystrix";
    }
}
