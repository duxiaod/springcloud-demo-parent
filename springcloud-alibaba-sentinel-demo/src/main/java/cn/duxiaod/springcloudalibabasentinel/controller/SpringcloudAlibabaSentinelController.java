package cn.duxiaod.springcloudalibabasentinel.controller;

import cn.duxiaod.springcloudalibabasentinel.service.SentinelTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringcloudAlibabaSentinelController {
    @Autowired
    private SentinelTestService service;
    @GetMapping(value = "/hello")
    public String hello() {
        return "Hello Sentinel";
    }

    @GetMapping(value = "/selectUserByName/{name}")
    public Object selectUserByName(@PathVariable(value = "name")String name) {
        return service.selectUserByName(name);
    }
}
