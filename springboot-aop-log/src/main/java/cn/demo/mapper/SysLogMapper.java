package cn.demo.mapper;

import cn.demo.entity.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface SysLogMapper extends BaseMapper<SysLog> {

}
