## RuntimeException

1. NullPointerException：空指针引用异常
2. ClassCaseException：类型转换异常
3. IllegalArgumentException：传递非法参数异常
4. IndexOutOfBoundsException：下标越界异常
5. NumberFormatException：数字格式化异常

## 非RuntimeException

1. ClassNotFundException：找不到指定class异常
2. IOEXception：IO操作异常
