## LinkedList

`LinkedList`底层是基于双向链表实现的，也是实现了`List`接口，所以也拥有List的一些特点（JDK1.7/1.8之后取消了循环，修改为双向链表）。

数据结构：

```java
private static class Node<E> {
	// 数据
    E item;
    // 后继节点
    Node<E> next;
    // 前驱节点
    Node<E> prev;

    Node(Node<E> prev, E element, Node<E> next) {
        this.item = element;
        this.next = next;
        this.prev = prev;
    }
}
```



### add方法

```java
public boolean add(E e) {
    linkLast(e);
    return true;
}

void linkLast(E e) {
    final Node<E> l = last;
    final Node<E> newNode = new Node<>(l, e, null);
    last = newNode;
    if (l == null)
        first = newNode;
    else
        l.next = newNode;
    size++;
    modCount++;
}
```

可见每次插入都是移动指针，和`ArrayList`每次拷贝数组来说效率较高。

### 查找方法

```java
public E get(int index) {
    checkElementIndex(index);
    return node(index).item;
}

Node<E> node(int index) {
    // assert isElementIndex(index);
	// 利用双向链表的特性，如果`index`离链表头比较近，就从头部遍历，否则就从节点尾部开始遍历，使用空间（空间）来换取时间。
    if (index < (size >> 1)) {
        Node<E> x = first;
        for (int i = 0; i < index; i++)
            x = x.next;
        return x;
    } else {
        Node<E> x = last;
        for (int i = size - 1; i > index; i--)
            x = x.prev;
        return x;
    }
}
```

- `node()`会以`O(n/2)`的性能去获取一个节点，如果节点的索引值大于链表大小的一半，那么将从尾节点开始遍历。

很明显这样查找的效率是非常低的，特别是当index的值越接近size值的一半时。

总结：

- `LinkedList`插入，删除都是移动指针效率很高。
- 查找需要遍历查询，效率很低。
