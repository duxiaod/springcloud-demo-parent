## ConcurrentHashMap实现原理

### JDK1.8实现

1.8中的ConcurrentHashMap数据结构和实现与1.7还是有着明细的差异。

其中抛弃了原有的`Segment`分段锁，而是采用了`CAS+synchronized`来保证并发安全性。

也将1.7中存放数据的HashEntry改为Node，但作用都相同。

其中`val``next`都使用了volatile修饰，保证了可见性。

### put方法

重点查看put函数

```java
final V putVal(K key, V value, boolean onlyIfAbsent) {
        if (key == null || value == null) throw new NullPointerException();
        // 1.计算key的hash值
        int hash = spread(key.hashCode());
        int binCount = 0;
        for (Node<K,V>[] tab = table;;) {
            // 2.f:用key定位到的数据
            Node<K,V> f; int n, i, fh;
            // 3.判断是否需初始化
            if (tab == null || (n = tab.length) == 0)
                tab = initTable();
            else if ((f = tabAt(tab, i = (n - 1) & hash)) == null) {
                // 4.f即为当前key定位出的Node，如果为空表示当前位置可以写入数据，利用CAS尝试写入，失败则自旋保证成功
                if (casTabAt(tab, i, null,
                             new Node<K,V>(hash, key, value, null)))
                    break;                   // no lock when adding to empty bin
            }
            // 5.如果当前位置hashcode == MOVED == -1，则需要进行扩容
            else if ((fh = f.hash) == MOVED)
                tab = helpTransfer(tab, f);
            else {
                V oldVal = null;
                // 6.条件都不满足，则利用synchronized锁写入数据
                synchronized (f) {
                    // 此处省略。。。。
                }
                if (binCount != 0) {
                    // 7.如果数量大于TREEIFY_THRESHOLD = 8 则要转换为红黑树
                    if (binCount >= TREEIFY_THRESHOLD)
                        treeifyBin(tab, i);
                    if (oldVal != null)
                        return oldVal;
                    break;
                }
            }
        }
        addCount(1L, binCount);
        return null;
    }
```



### get方法

- 根据计算出来的hashcode寻址，如果就在桶上那么直接返回值。
- 如果是红黑树就按照数的方式获取值。
- 都不满足那就按照链表的方式遍历获取值。

### 总结

1.8在1.7的数据结构上做了大的改动，采用了红黑树之后保证查询效率（`O(logn)`）,甚至取消了ReentrantLock改为了synchronized，这样可以看出新版的JDK中对synchronized优化是很到位的。