## HashMap



## HashMap和HashTable的区别

1. 线程是否安全：HashMap是线程不安全的；HashTable是线程安全的，因为HashTable方法内部都使用`synchronized`修饰。（保证线程安全推荐使用ConcurrentHashMap）
2. 效率：线程安全的问题，HashMap的效率高于HashTable。另外HashTable基本被淘汰了，不推荐使用。
3. 对`NULL KEY`和`NULL VALUE`的支持：HashMap可以存null的key和value，单null作为key只能只有一个，作为value可以有多个；HashTable不允许有null的key和value。
4. 初始容量和每次扩容的大小不同：
   - 创建时如果不指定初始值，HashMap默认容量大小为16，之后每次扩容，容量变为原来的2倍；HashTable初始容量为11，之后每次扩容，变为原来容量的2n+1。
   - 创建时如果给定了初始容量：HashMap会将其扩充为2的幂次方大小（HashMap中的`tableSizeFor()`方法保证）；HashTable会直接使用给定的大小。
5. 底层数据结构：JDK1.8之后的HashMap在解决哈希冲突时有了较大的变化，当链表长度大于阈值（默认为8）（将链表转为红黑树前会判断，如果当前数组的长度小于64，那么会先进行数组扩容，而不是转为红黑树）时，将链表转为红黑树，以减少搜索时间。HashTable没有这种机制。

## HashSet

底层基于HashMap实现，除了`clone()`、序列化和反序列化方法，其他基本使用了HashMap方法。

主要成员变量：

```java
private transient HashMap<E,Object> map;

// Dummy value to associate with an Object in the backing Map
private static final Object PRESENT = new Object();
```

