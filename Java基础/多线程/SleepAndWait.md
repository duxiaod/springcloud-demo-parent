## sleep和wait区别

1. sleep是线程的方法`Thread.sleep(1000)`，wait是Object的方法
2. sleep不会释放锁，而wait会释放锁
3. sleep不依赖与同步器`synchronized`,但是wait需要`synchronized`关键字
4. sleep不需要被唤醒（休眠之后推出阻塞），但是wait需要（不指定时间需要被人中断）

