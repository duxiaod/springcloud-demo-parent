# 1.CPU过高
通过 top 命令找到CPU消耗最高的进程，并记下进程ID；
```shell
top
```
再次通过 top -Hp [进程ID]找到CPU消耗过高的线程ID，并记住线程ID；
```shell
top -Hp 17850
```
转换线程ID（10进制转换为16进制）
```shell
printf '%x\n' [10进制线程ID]
```
通过jdk提供的 jstack 工具定位CPU占用线程
```shell
jstack [进程ID]|grep [线程16进制] -A 30
```
# 2.内存问题排查
通常内存的问题就是GC的问题，因为Java的内存由GC来管理。有两种情况，一种是内存溢出了，一种是内存没有溢出，单是GC不健康。

内存溢出的情况可以加上启动参数：+XX:HeapDumpOnOutOfMemoryError 参数，在程序内村溢出时输入 dump 文件。

有了 dump 文件，就可以通过 dump 分析工具进行分析了，比如常用的MAT，Jprofile，jvisualvm等工具都可以分析。能定位到具体哪里溢出，哪里创建了大量对象等信息。</br>

第二种情况相对比较复杂，GC不健康问题：

通常一个健康的GC是什么样的？根据经验，YGC一般5秒一次左右，一次50毫秒左右，FGC最好没有，CMS GC一天一次左右。

GC的优化有两个维度，一个是频率，一个是时长。

FGC的原因：1是OLD区内存不够导致FGC，2是元数据区内存不够，3是 System.gc()，4是 jmap 或者 jcmd ，5是 CMS Promotion failed ，6JVM基于悲观策略认为这次 YGC 后 Old 区无法容纳晋升的对象，因此取消 YGC，提前 FGC。

通常的优化点是：Old 区内存不够导致 FGC。如果FGC后还有大对象，说明OLD区过小，应该扩大OLD区，如果FGC后效果很好，说明OLD区存在了很多短命的对象，优化的点就是这些对象在新生代就被YGC掉，通常的做法是增大新生代，如果有大而短命的对象，通过参数设置对象的大小，不要让这些对象进入 OLD 区，还需要检查晋升年龄是否过小。
如果 YGC 后，有大量对象因为无法进入 Survivor 区从而提前晋升，这时应该增大 Survivor 区，但是不宜过大。

常用命令

1. jps：查看当前运行的java进程，可以查看到线程的ID、入口类、启动参数等
```shell
jps 
```
2. jstat：查看指定虚拟机进程的各种运行状态信息，包含：类装载、垃圾收集、运行期编译状况等信息
```shell
jstat -gc 21044
```
3. jinfo：可以实时查看虚拟机的各项参数，比如：System.getProperties()的参数，并且可以修改其值
```shell
jinfo 21044
```
4. jmap：生成堆内存的dump日志
```shell
jmap -dump:live,format=b,file=heap.bin 21044
```
5. jstack：查看指定虚拟机各个线程的运行状态
```shell
jstack 21044                                                       
```
