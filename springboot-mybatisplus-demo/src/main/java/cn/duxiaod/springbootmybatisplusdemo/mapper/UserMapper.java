package cn.duxiaod.springbootmybatisplusdemo.mapper;

import cn.duxiaod.springbootmybatisplusdemo.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface UserMapper extends BaseMapper<User> {

}
