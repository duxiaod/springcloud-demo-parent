package cn.duxiaod.springbootmybatisplusdemotest;

import cn.duxiaod.springbootmybatisplusdemo.SpringbootMybatisPlusApplication;
import cn.duxiaod.springbootmybatisplusdemo.entity.User;
import cn.duxiaod.springbootmybatisplusdemo.mapper.UserMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringbootMybatisPlusApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SampleTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testList() {
        System.out.println(("----- testList method test ------"));
        List<User> userList = userMapper.selectList(null);
        Assert.assertEquals(5, userList.size());
        userList.forEach(System.out::println);
    }
    @Test
    public void testInsert() {
        System.out.println(("----- testInsert method test ------"));
        User user = new User();
        user.setId(6L);
        int result = userMapper.insert(user);
        Assert.assertEquals(1, result);
    }
    @Test
    public void testUpdate() {
        System.out.println(("----- testUpdate method test ------"));
        User user = new User();
        user.setId(6L);
        user.setAge(21);
        int result = userMapper.updateById(user);
        Assert.assertEquals(1, result);
    }
    @Test
    public void testDeleteById() {
        System.out.println(("----- testDeleteById method test ------"));
        int result = userMapper.deleteById(6L);
        Assert.assertEquals(1, result);
    }

}