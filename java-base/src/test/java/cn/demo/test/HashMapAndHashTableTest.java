package cn.demo.test;

import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;

public class HashMapAndHashTableTest {

    @Test
    public void hashMapTest(){
        HashMap<String, String> hashMap = new HashMap<>(2);
        hashMap.put(null,null);
    }

    @Test
    public void hashTableTest(){
        Hashtable<String, String> hashtable = new Hashtable<>();
        // key value均不能为null
        //hashtable.put(null,null);
        hashtable.put("key","value");
        System.out.println(hashtable);
    }

    @Test
    public void hashSetTest(){
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("1");
        boolean result = hashSet.add("1");
        System.out.println(result);
        System.out.println(hashSet);
    }
}
