package cn.demo.test;

import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Vector;

public class ArrayListAndVectorTest {
    @Test
    public void arrayListAddTest(){
        ArrayList<String> arrayList = new ArrayList<>(1);
        arrayList.add("1");
        arrayList.add(0,"1");
        arrayList.add(2,"1");
        arrayList.add(null);
        System.out.println(arrayList);
    }

    @Test
    public void vectorAddTest(){
        Vector<String> vector = new Vector<>();
        vector.add("1");
        vector.add(0,"1");
        vector.add(2,"1");
        System.out.println(vector);
    }
}
