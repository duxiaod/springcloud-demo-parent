package cn.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author duxiaod
 * @date 2021/07/25 20:26
 **/
@SpringBootApplication
public class JavaBaseApplication {
    public static void main(String[] args) {
        SpringApplication.run(JavaBaseApplication.class,args);
    }
}
