package cn.demo.base;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.*;

/**
 * 延时执行
 */
public class TimerTest {
    public static void main(String[] args) {
        // 1.使用Timer（不建议使用）
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        }, 4000);
        // 2.使用延时线程池
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
        Thread thread = new Thread(()->{
            System.out.println(Thread.currentThread().getName());
        });
        executorService.schedule(thread,4, TimeUnit.SECONDS);
    }
}
