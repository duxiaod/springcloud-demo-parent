package cn.demo.thread;

import java.util.concurrent.TimeUnit;

/**
 * Sleep and wait
 *
 * @author duxiaod
 * @date 2021/07/28 14:30
 **/
public class SleepAndWaitTest {

    public static void main(String[] args) {

        new Thread(()->{
            synchronized (SleepAndWaitTest.class){
                try {
                    SleepAndWaitTest.class.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"wait end");
            }
        }).start();

        new Thread(()->{
            synchronized (SleepAndWaitTest.class){
                try {
                    SleepAndWaitTest.class.notify();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"notiry end");
            }
        }).start();

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
