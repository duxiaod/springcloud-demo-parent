package cn.demo.thread;

import java.util.concurrent.TimeUnit;

/**
 * 终止线程
 *
 * @author duxiaod
 * @date 2021/07/24 20:43
 **/
public class StopThread implements Runnable{
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new StopThread());
        thread.start();

        System.out.println("main 线程正在运行") ;
        TimeUnit.MILLISECONDS.sleep(10);
        thread.interrupt();
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()){
            System.out.println(Thread.currentThread().getName()+"运行中");
        }
        System.out.println("线程终止");
    }
}
