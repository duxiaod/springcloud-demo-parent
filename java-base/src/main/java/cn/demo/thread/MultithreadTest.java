package cn.demo.thread;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class MultithreadTest{

    public static void main(String[] args) {
        createThreadTest();
    }

    public static void createThreadTest(){
        // 1.实现Thread接口
        new Thread1().start();
        // 2.实现Runabled接口
        new Thread(new Thread2(),"线程2").start();
        // 3.实现Callable接口
        Callable callable = new Thread3();
        FutureTask futureTask = new FutureTask<>(callable);
        new Thread(futureTask,"线程3").start();
        try {
            System.out.println(futureTask.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        // 4.使用线程池（使用join方法或者单线程池以保证线程的有序进行）
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Thread t1 = new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+":t1");
        },"t1");
        Thread t2 = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+":t2");
        },"t2");
        Thread t3 = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+":t3");
        },"t3");
        executorService.submit(t1);
        executorService.submit(t2);
        executorService.submit(t3);
//        executorService.shutdown();

        Thread t4 = new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+":t4");
        },"t4");
        Thread t5 = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+":t5");
        },"t5");
        Thread t6 = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+":t6");
        },"t6");
        try {
            t4.start();
            t4.join();
            t5.start();
            t5.join();
            t6.start();
        }catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("分割线");
        AtomicInteger count = new AtomicInteger();
        for (int i = 0; i < 1000; i++) {
            new Thread(()->{
                count.incrementAndGet();
                System.out.println(Thread.currentThread().getName());
            }).start();
        }
        // 自旋等待其他线程运行完毕
        for (;;){
            if(count.get()==1000){
                System.out.println("ok，结果为："+count.get());
                break;
            }
        }
        System.out.println("其他线程已经运行完毕");

        System.out.println("分割线");
        CountDownLatch countDownLatch = new CountDownLatch(1000);
        for (int i = 0; i < 1000; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName());
                countDownLatch.countDown();
            }).start();
        }
        try {
            countDownLatch.await();
            System.out.println("ok");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 多个线程相互等待
        CyclicBarrier cyclicBarrier = new CyclicBarrier(2);
        for (int i = 0; i < 2; i++) {
            new Thread(()->{
                try {
                    System.out.println(Thread.currentThread().getName());
                    TimeUnit.SECONDS.sleep(1);
                    cyclicBarrier.await();
                    System.out.println(Thread.currentThread().getName()+"执行结束");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();
        }

    }

    static class Thread1 extends Thread{
        @Override
        public void run() {
            super.setName("线程1");
            System.out.println(Thread.currentThread().getName());
        }
    }

    static class Thread2 implements Runnable{

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName());
        }
    }

    static class Thread3 implements Callable<Boolean>{

        @Override
        public Boolean call() throws Exception {
            System.out.println(Thread.currentThread().getName());
            return Boolean.TRUE;
        }
    }
}
