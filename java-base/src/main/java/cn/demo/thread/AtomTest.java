package cn.demo.thread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 原子类测试
 *
 * @author duxiaod
 * @date 2021/07/26 15:25
 **/
public class AtomTest {

    public static void main(String[] args) throws InterruptedException {
        AtomicInteger integer = new AtomicInteger();
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                integer.incrementAndGet();
            }).start();
        }
        TimeUnit.SECONDS.sleep(5);
        System.out.println(integer.get());
    }
}
