### Redis支持的数据类型
- String字符串：格式，get/set key value

- Hash（哈希）：格式，hmget/hmset name key1 value1 key2 value2

- List（列表）：

  格式: lpush name value

  在 key 对应 list 的头部添加字符串元素

  格式: rpush name value

  在 key 对应 list 的尾部添加字符串元素

  格式: lrem name index

  key 对应 list 中删除 count 个和 value 相同的元素

  格式: llen name 

  返回 key 对应 list 的长度

- Set（集合）：格式，sadd name value

- zset（sorted set：有序集合）：格式，zadd name score value，根据score排序，可重复

### Redis 有哪些架构模式？讲讲各自的特点

1. 单机版

   特点：简单。问题：内存容量有限处理能力有限；无法保证高可用。

2. 主从复制

   特点：master/slave 数据相同；降低master库读压力转到从库。问题：无法保证高可用；master库写压力未缓解。

3. 哨兵

   特点：保证高可用；监控各个节点；自动故障迁移。问题：主从模式，切换需要时间丢数据；没有解决master写压力

4. 集群直连

   特点：无中心架构，少了proxy代理层；数据按照solt分布在多个节点，节点间数据共享，可动态调整数据分布；可扩展性，可线性扩展到1000个节点，节点可动态添加或删除；高可用性，部分节点不可用时集群仍可用，通过增加slave做备份数据副本；实现故障自动failover，节点之间通过gossip协议交换状态信息，用投票机制完成Slave和Master的角色提升。

   缺点：资源隔离性较差，容易出现相互影响的情况，数据通过异步复制，不保证数据的强一致性。

### Redis持久化如何实现？将内存中的数据异步写入磁盘，两种方式?

RDB（默认）和AOF

RDB（快照持久化）持久化原理：实际上就是把数据以快照的形式保存下来。`save`（会阻塞客户端命令）和`bgsave`（不会阻塞客户端命令）两种触发方式。

AOP（append only file）持久化：`AOF`工作机制很简单，redis会将每一个收到的写命令都通过write函数追加到文件中。通俗的理解就是日志记录。三种触发机制：一是每修改同步always：同步持久化 每次发生数据变更会被立即记录到磁盘 性能较差但数据完整性比较好；二是每秒同步everysec：异步操作，每秒记录 如果一秒内宕机，有数据丢失；三是不同no：从不同步使用过

### Redis做异步队列么，你是怎么用的？有什么缺点？

一般使用list数据结构作为队列，`rpush`生产消息，`lpop`消费消息，当lpop没有消息时，需要sleep一会儿重试。缺点：在消费者下线的情况下，生产的消息会丢失，建议使用专业的消息队列如`rabbitmq`等。

### Redis实现分布式锁

1. 加锁

   ```shell
   SET lock_key random_value NX PX 5000
   ```

   值得注意的是：
    `random_value` 是客户端生成的唯一的字符串。
    `NX` 代表只在键不存在时，才对键进行设置操作。
    `PX 5000` 设置键的过期时间为5000毫秒。

   这样，如果上面的命令执行成功，则证明客户端获取到了锁。

2. 释放锁

   释放锁的过程就是删除key，但是也并不是乱删除，不能客户端1的请求删除了客户端2的锁，此时<font color='red'>`random_value`</font>的价值就体现出来了。

   为了保证释放锁操作的原子性，我们需要使用LUA脚本完成这个操作。先判断当前操作的字符串是否与传入的值相等，是的话就删除key，释放锁成功。

   ```lua
   if redis.call('get',KEYS[1]) == ARGV[1] then 
       return redis.call('del',KEYS[1]) 
   else
       return 0 
   end
   ```

   

