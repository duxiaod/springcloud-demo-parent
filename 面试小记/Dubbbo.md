### 什么是dubbo

Dubbo 是阿里巴巴开发用来治理服务中间件、资源调度和治理中心的管理工具。

### 服务注册与发现的流程图

<img src="https://gitee.com/duxiaod/img-box/raw/master/img/dubbo%E6%9C%8D%E5%8A%A1%E5%8F%91%E7%8E%B0%E4%B8%8E%E6%B3%A8%E5%86%8C.png" alt="dubbo服务发现与注册" style="zoom:67%;" />

### 【dubbo节点角色说明】

- Provider：暴露服务的服务提供方（service）
- Consumer：调用远程服务的服务消费方（web）
- Registry：服务注册与发现的注册中心（zookeeper）
- Monitor：统计服务调用次数和调用是时间的的监控中心。
- Container：服务运行容器（tomcat容器，spring容器）

### 【dubbo的注册原理】zookeeper流程

- 服务提供者启动时向`/dubbo/接口限定路径(com.demo.CostService)/provides`下写入自己的url地址
- 服务消费者启动时订阅`/dubbo/接口限定路径(com.demo.CostService)/provides`目录下的服务提供方url
- 并向`/dubbo/接口限定路径(com.demo.CostService)/consumers`目录写入自己的url地址
- 监控中心启动时订阅`/dubbo/接口限定路径(com.demo.CostService)`目录下的所有消费者和提供者的ur地址

### 【注册中心】

- Multicast：注册中心不需要启动任何中心节点，只要广播地址一样，就可以互相发现。
- Zookeeper：是Apache Hadoop项目的子项目，是一个数形的目录服务，支持变更推送，适合作为Dubbo服务的注册注册中心，工业强度较高，可用于生产环境，并推荐使用。
- Redis：基于Redis实现的注册中心。
- Simple：其本身就是一个Dubbo服务，可以减少第三方依赖，使整体通讯方式一致。
- Nacos：

### 【dubbo、zookeeper流程，从生产者到消费者】

- 生产者消费者都要进行dubbo的配置，都需要注册zookeeper主机地址。
- 生产者要配置dubbo使用的协议（默认dubbo）和端口号用来暴露服务。
- 生产者定义接口和实现类，并在配置文件中注册服务。
- 生产者在启动时会自动把注册的接口的信息转化为一个url。
- 并通过`hessian`二进制序列化存储到zookeeper节点中。
- 消费者在配置文件中引入要使用的接口。
- 消费者启动时会从zookeeper中获取与引用的接口匹配的url。
- 并把自己的信息留在zookeeper中。
- 服务者和消费者在zookeeper的信息都会被监控中心`monitor`获取到。
- 可以通过`monitor`服务对zookeeper中的内容进行管理。
