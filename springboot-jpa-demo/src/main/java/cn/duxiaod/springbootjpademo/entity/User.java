package cn.duxiaod.springbootjpademo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
@Entity
public class User {
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "name", nullable = true, length = 32)
    private String name;
    @Column(name = "age", nullable = true, length = 11)
    private Integer age;
    @Column(name = "email", nullable = true, length = 50)
    private String email;
}
