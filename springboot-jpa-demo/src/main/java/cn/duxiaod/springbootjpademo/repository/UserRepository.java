package cn.duxiaod.springbootjpademo.repository;

import cn.duxiaod.springbootjpademo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
    /**
     * 根据用户名查找用户信息
     * @param name
     * @return
     */
    User findUserByName(String name);
}
