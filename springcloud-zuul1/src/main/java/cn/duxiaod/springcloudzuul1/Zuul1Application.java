package cn.duxiaod.springcloudzuul1;

import cn.duxiaod.springcloudzuul1.filter.TokenFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableZuulProxy
public class Zuul1Application {
    public static void main(String[] args) {
        SpringApplication.run(Zuul1Application.class,args);
    }
}
