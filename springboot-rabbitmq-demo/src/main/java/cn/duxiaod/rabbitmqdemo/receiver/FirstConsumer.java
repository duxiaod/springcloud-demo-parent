package cn.duxiaod.rabbitmqdemo.receiver;

import cn.duxiaod.rabbitmqdemo.config.QueueConfig;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消息消费者1
 */
@Component
public class FirstConsumer {

    @RabbitListener(queues = {QueueConfig.FIRST_QUEUE,QueueConfig.SECOND_QUEUE}, containerFactory = "rabbitListenerContainerFactory")
    public void handleMessage(String message) throws Exception {
        // 处理消息
        System.out.println("FirstConsumer {} handleMessage :"+message);
    }
}
