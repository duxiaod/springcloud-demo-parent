package cn.duxiaod.rabbitmqdemo.test;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 生产者
 *
 * @author duxiaod
 * @date 2021/07/28 15:00
 **/
public class Provider {

    public static void main(String[] args) throws IOException, TimeoutException {

        // 创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername("guest");
        factory.setPassword("guest");
        // 设置RabbitMQ地址
        factory.setHost("localhost");
        // 建立到代理服务器的连接
        Connection connection = factory.newConnection();
        // 创建信道
        Channel channel = connection.createChannel();
        // 声明交换价
        String exchangeName = "hello-exchange";
        channel.exchangeDeclare(exchangeName,"direct",true);

        String routingKey = "hello";
        // 发布消息
        for (int i = 0; i < 10; i++) {
            byte[] messageBody= ("hi"+i).getBytes();
            channel.basicPublish(exchangeName,routingKey,null,messageBody);
        }
        channel.close();
        connection.close();
    }
}
