package cn.duxiaod.rabbitmqdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootRabbbitMQDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootRabbbitMQDemoApplication.class,args);
    }
}
