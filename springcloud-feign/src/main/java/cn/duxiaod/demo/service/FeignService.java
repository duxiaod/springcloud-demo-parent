package cn.duxiaod.demo.service;

import cn.duxiaod.demo.service.hystrix.FeignServiceHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "springcloud-eureka-client",fallback = FeignServiceHystrix.class)
public interface FeignService {

    @RequestMapping(value = "api/userInfo/{uid}",method = RequestMethod.GET)
    public String userInfo(@PathVariable("uid")String uid);

}
