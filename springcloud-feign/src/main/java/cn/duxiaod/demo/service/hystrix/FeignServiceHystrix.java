package cn.duxiaod.demo.service.hystrix;

import cn.duxiaod.demo.service.FeignService;
import org.springframework.stereotype.Component;

@Component
public class FeignServiceHystrix implements FeignService {
    @Override
    public String userInfo(String uid) {
        return "userInfo by uid :"+uid+" error! from by Hystrix!";
    }
}
