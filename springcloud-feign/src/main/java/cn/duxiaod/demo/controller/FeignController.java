package cn.duxiaod.demo.controller;

import cn.duxiaod.demo.service.FeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeignController {
    @Autowired
    private FeignService feignService;

    @GetMapping(path = "api/userInfo/{uid}")
    public String userInfo(@PathVariable("uid")String uid){
        return feignService.userInfo(uid)+" from feign";
    }
}
