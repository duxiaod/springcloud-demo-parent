package cn.duxiaod.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    @GetMapping(path = "auth/logout")
    public String logout(){
        return "登出成功";
    }

    @GetMapping(path = "auth/login")
    public String login(){
        return "登入成功";
    }
}
