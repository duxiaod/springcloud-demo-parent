package cn.demo.redis.controller;

import cn.demo.redis.entity.User;
import cn.demo.redis.service.UserService;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/list")
    public List<User> list(){
        Wrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        return userService.list(queryWrapper);
    }

    @GetMapping("/selectById")
    public User selectById(){
        return userService.selectById(1L);
    }

    @PostMapping("/update")
    public int update(User user){
        user.setId(1L);
        user.setName("露露");
        return userService.update(user);
    }
}
