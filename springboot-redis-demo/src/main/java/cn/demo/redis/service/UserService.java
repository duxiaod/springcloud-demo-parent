package cn.demo.redis.service;

import cn.demo.redis.entity.User;
import cn.demo.redis.mapper.UserMapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public List<User> list(Wrapper<User> queryWrapper){
        return userMapper.selectList(queryWrapper);
    }

    @Cacheable(cacheNames="user_key", key="#id")
    public User selectById(Long id){
        return userMapper.selectById(id);
    }

    @Caching(evict = {@CacheEvict(value = "user_key",allEntries = true),
            @CacheEvict(value = "user_key_value",allEntries = true)})
    public int update(User user){
        return userMapper.updateById(user);
    }
}
