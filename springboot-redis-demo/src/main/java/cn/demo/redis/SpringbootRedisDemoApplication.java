package cn.demo.redis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("cn.demo.redis.mapper")
public class SpringbootRedisDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootRedisDemoApplication.class,args);
    }
}
