package cn.duxiaod.springcloudssoserverdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcloudSSOServerDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringcloudSSOServerDemoApplication.class,args);
    }
}
