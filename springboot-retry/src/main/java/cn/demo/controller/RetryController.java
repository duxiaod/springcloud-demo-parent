package cn.demo.controller;

import cn.demo.service.RetryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/retry")
public class RetryController {
    @Autowired
    private RetryService retryService;

    @GetMapping("transfer")
    public String transfer() {
        try {
            retryService.retryTransferAccounts(1,2,10);
            return "yes";
        } catch (Exception e) {
            e.printStackTrace();
            return "no";
        }
    }
}