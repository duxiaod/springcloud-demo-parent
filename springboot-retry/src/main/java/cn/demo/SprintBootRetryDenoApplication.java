package cn.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@EnableRetry
@SpringBootApplication
@MapperScan("cn.demo.cn.demo.redis.mapper")
public class SprintBootRetryDenoApplication {
    public static void main(String[] args) {
        SpringApplication.run(SprintBootRetryDenoApplication.class,args);
    }
}
