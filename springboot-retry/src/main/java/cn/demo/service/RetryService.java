package cn.demo.service;

public interface RetryService {
    void retryTransferAccounts(int fromAccountId, int toAccountId, int money) throws Exception;
}
