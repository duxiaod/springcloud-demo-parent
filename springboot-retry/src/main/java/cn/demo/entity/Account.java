package cn.demo.entity;

import lombok.Data;

@Data
public class Account {
    private Long id;
    private int balance;
}
