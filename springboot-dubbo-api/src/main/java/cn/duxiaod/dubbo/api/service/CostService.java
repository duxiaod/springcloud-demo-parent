package cn.duxiaod.dubbo.api.service;


public interface CostService {
    /**
     * 成本增加接口
     * @param cost
     * @return
     */
    public Integer add(int cost);
}
